/* eslint-disable no-param-reassign */
import { Model, DataTypes, Op } from 'sequelize';
import { pick, isNil, omitBy } from 'lodash';
import moment from 'moment-timezone';

import postgres from '../../config/postgres';
import { serviceName } from '../../config/vars';

/**
 * Create connection
 */
class Report extends Model { }
const { sequelize } = postgres;

const PUBLIC_FIELDS = [
    'channel',
    'price_book',
    'total_value_1',
    'total_value_2',
    'total_value_3',
    'total_value_4',
    'total_value_5',
    'total_value_6',
    'total_value_7',
    'total_value_8',
    'total_value_9',
    'total_value_10',
    'total_value_11',
    'total_value_12',
    'total_value_13',
    'total_value_14',
    'total_value_15',
    'total_quantity_1',
    'total_quantity_2',
    'created_by',
    'created_time'
];

/**
 * Report Schema
 * @public
 */
Report.init(
    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        channel: {
            type: DataTypes.INTEGER,
            allowNull: false // id | name
        },
        price_book: {
            type: DataTypes.INTEGER,
            defaultValue: null
        },
        store: {
            type: DataTypes.JSONB,
            allowNull: false
        },
        total_value_1: {
            // Tổng tiền hàng không tính trả hàng
            type: DataTypes.DECIMAL,
            defaultValue: 0
        },
        total_value_2: {
            // Tổng tiền hàng tính cả trả hàng
            type: DataTypes.DECIMAL,
            defaultValue: 0
        },
        total_value_3: {
            // Chiết khấu hóa đơn không tính trả hàng
            type: DataTypes.DECIMAL,
            defaultValue: 0
        },
        total_value_4: {
            // Giá trị hàng bán bị trả lại
            type: DataTypes.DECIMAL,
            defaultValue: 0
        },
        total_value_5: {
            // Doanh thu
            type: DataTypes.DECIMAL,
            defaultValue: 0
        },
        total_value_6: {
            // Giá vốn hàng bán
            type: DataTypes.DECIMAL,
            defaultValue: 0
        },
        total_value_7: {
            // Chi phí voucher
            type: DataTypes.DECIMAL,
            defaultValue: 0
        },
        total_value_8: {
            // Phí giao hàng
            type: DataTypes.DECIMAL,
            defaultValue: 0
        },
        total_value_9: {
            // Giá trị xuất hủy
            type: DataTypes.DECIMAL,
            defaultValue: 0
        },
        total_value_10: {
            //  Giá trị thanh toán bằng điểm
            type: DataTypes.DECIMAL,
            defaultValue: 0
        },
        total_value_11: {
            // Phí chi trả lương Nhân viên
            type: DataTypes.DECIMAL,
            defaultValue: 0
        },
        total_value_12: {
            // Thu nhập khác
            type: DataTypes.DECIMAL,
            defaultValue: 0
        },
        total_value_13: {
            // Phí trả hàng
            type: DataTypes.DECIMAL,
            defaultValue: 0
        },
        total_value_14: {
            // Chi phí khác
            type: DataTypes.DECIMAL,
            defaultValue: 0
        },
        total_value_15: {
            // Tổng giảm giá tính cả khi trả hàng
            type: DataTypes.DECIMAL,
            defaultValue: 0
        },
        total_value_16: {
            // Tổng doanh thu tính cả khi trả hàng
            type: DataTypes.DECIMAL,
            defaultValue: 0
        },
        total_value_17: {
            // Tổng lợi nhuận gộp tính cả khi trả hàng
            type: DataTypes.DECIMAL,
            defaultValue: 0
        },
        total_quantity_1: {
            // Số lượng đơn đã bán
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        total_quantity_2: {
            // Số lượng đơn đã trả
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        year: {
            type: DataTypes.INTEGER,
            length: 0,
            defaultValue: 0
        },
        quarter: {
            type: DataTypes.INTEGER,
            length: 0,
            defaultValue: 0
        },
        month: {
            type: DataTypes.INTEGER,
            length: 0,
            defaultValue: 0
        },
        hours: {
            type: DataTypes.INTEGER,
            length: 0,
            defaultValue: 0
        },

        // manager
        is_delivery: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        is_discount: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        is_return: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        created_time: {
            type: DataTypes.DATEONLY,
            length: 0,
            defaultValue: 0
        },
        created_by: {
            type: DataTypes.JSONB,
            defaultValue: null // id | name
        },
        created_at: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        updated_at: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        }
    },
    {
        timestamps: false,
        sequelize: sequelize,
        schema: serviceName,
        modelName: 'report',
        tableName: 'tbl_reports'
    }
);

/**
 * Register event emiter
 */
Report.Events = {
    Report_CREATED: `${serviceName}.Report.created`,
    Report_UPDATED: `${serviceName}.Report.updated`,
    Report_DELETED: `${serviceName}.Report.deleted`,
};
Report.EVENT_SOURCE = `${serviceName}.Report`;

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
Report.addHook('beforeCreate', () => { });

Report.addHook('afterUpdate', () => { });

Report.addHook('afterDestroy', () => { });

/**
 * Check min or max in condition
 * @param {*} options
 * @param {*} field
 * @param {*} type
 */
function checkMinMaxOfConditionFields(options, field, type = 'Number') {
    let _min = null;
    let _max = null;

    // Transform min max
    if (type === 'Date') {
        _min = new Date(options[`min_${field}`]);
        _min.setHours(0, 0, 0, 0);

        _max = new Date(options[`max_${field}`]);
        _max.setHours(23, 59, 59, 999);
    } else {
        _min = parseFloat(options[`min_${field}`]);
        _max = parseFloat(options[`max_${field}`]);
    }

    // Transform condition
    if (
        !isNil(options[`min_${field}`]) ||
        !isNil(options[`max_${field}`])
    ) {
        if (
            options[`min_${field}`] &&
            !options[`max_${field}`]
        ) {
            options[field] = {
                [Op.gte]: _min
            };
        } else if (
            !options[`min_${field}`] &&
            options[`max_${field}`]
        ) {
            options[field] = {
                [Op.lte]: _max
            };
        } else {
            options[field] = {
                [Op.gte]: _min || 0,
                [Op.lte]: _max || 0
            };
        }
    }

    // Remove first condition
    delete options[`max_${field}`];
    delete options[`min_${field}`];
}

/**
 * Load query
 * @param {*} params
 */
function filterConditions(params) {
    const options = omitBy(params, isNil);

    if (options.staffs) {
        options['created_by.id'] = {
            [Op.in]: options.staffs.split(',')
        };
    }
    delete options.staffs;

    if (options.price_book) {
        options.price_book = {
            [Op.in]: options.price_book.split(',')
        };
    } else {
        delete options.price_book;
    }


    if (options.is_delivery) {
        options.is_delivery = {
            [Op.in]: options.is_delivery.split(',')
        };
    } else {
        delete options.is_delivery;
    }

    if (options.type === 'discount') {
        options.is_discount = true;
    }

    if (options.type === 'return') {
        options.is_return = {
            [Op.is]: true
        };
    }
    delete options.type;


    if (options.channels) {
        options.channel = {
            [Op.in]: options.channels.split(',')
        };
    }
    delete options.channels;

    if (options.stores) {
        options['store.id'] = {
            [Op.in]: options.stores.split(',')
        };
    }
    delete options.stores;

    if (options.years) {
        options.year = {
            [Op.eq]: options.years
        };
    }
    delete options.years;

    // date filters
    checkMinMaxOfConditionFields(options, 'created_time', 'Date');

    return options;
}

/**
 * Load sort query
 * @param {*} sort_by
 * @param {*} order_by
 */
function sortConditions({ date_time, order_by }) {
    let sort = null;
    if (!order_by) {
        order_by = 'asc';
    }
    switch (date_time) {
        case 'year':
            sort = ['year', order_by];
            break;
        case 'quarter':
            sort = ['quarter', order_by];
            break;
        case 'month':
            sort = ['month', order_by];
            break;

        default: sort = ['month', 'ASC'];
            break;
    }
    return sort;
}

/**
 * Load sort query
 * @param {*} sort_by
 * @param {*} order_by
 */
function groupConditions(date_time) {
    let group = [];
    switch (date_time) {
        case 'year':
            group = ['year'];
            break;
        case 'quarter':
            group = ['quarter'];
            break;
        case 'month':
            group = ['month'];
            break;
        default: group = ['month'];
            break;
    }
    return group;
}

/**
 * Transform postgres model to expose object
 */
Report.transform = (params) => {
    const transformed = {};
    const fields = [
        'channel',
        'price_book',
        'store',
        'total_value_1',
        'total_value_2',
        'total_value_3',
        'total_value_4',
        'total_value_5',
        'total_value_6',
        'total_value_7',
        'total_value_8',
        'total_value_9',
        'total_value_10',
        'total_value_11',
        'total_value_12',
        'total_value_13',
        'total_value_14',
        'total_value_15',
        'total_value_16',
        'total_value_17',
        'total_quantity_1',
        'total_quantity_2',
        'created_by',
        'created_time',
        'hours',
        'month',
        'quarter',
        'year'
    ];
    fields.forEach((field) => {
        transformed[field] = params[field];
    });

    // pipe date
    const dateFields = [
        'created_at',
        'updated_at'
    ];
    dateFields.forEach((field) => {
        if (params[field]) {
            transformed[field] = moment(params[field]).unix();
        } else {
            transformed[field] = null;
        }
    });

    return transformed;
};

/**
 * Get list sale report by time
 *
 * @public
 * @param {Parameters} params
 */
Report.list = async ({
    staffs,
    price_book,
    channels,
    stores,
    type,
    is_delivery,
    min_created_time,
    max_created_time,
    date_time,

    // sort
    skip = 0,
    limit = 20,
}) => {
    const options = filterConditions({
        staffs,
        price_book,
        channels,
        stores,
        is_delivery,
        type,
        min_created_time,
        max_created_time
    });
    let group = ['created_time'];
    let sort = ['created_time', 'asc'];
    let newAttributes = ['created_time'];
    let attributes = [
        [sequelize.fn('SUM', sequelize.col('total_value_1')), 'total_value_1'], // Tổng tiền hàng không tính trả hàng
        [sequelize.fn('SUM', sequelize.col('total_value_2')), 'total_value_2'], // Tổng tiền hàng tính cả trả hàng
        [sequelize.fn('SUM', sequelize.col('total_value_3')), 'total_value_3'], // Chiết khấu hóa đơn không tính trả hàng
        [sequelize.fn('SUM', sequelize.col('total_value_4')), 'total_value_4'], // Giá trị hàng bán bị trả lại
        [sequelize.fn('SUM', sequelize.col('total_value_5')), 'total_value_5'], // Doanh thu
        [sequelize.fn('SUM', sequelize.col('total_value_6')), 'total_value_6'], // Giá vốn hàng bán
        [sequelize.fn('SUM', sequelize.col('total_value_7')), 'total_value_7'], // Chi phí voucher
        [sequelize.fn('SUM', sequelize.col('total_value_8')), 'total_value_8'], // Phí giao hàng
        [sequelize.fn('SUM', sequelize.col('total_value_9')), 'total_value_9'], // Giá trị xuất hủy
        [sequelize.fn('SUM', sequelize.col('total_value_10')), 'total_value_10'], //  Giá trị thanh toán bằng điểm
        [sequelize.fn('SUM', sequelize.col('total_value_11')), 'total_value_11'], // Phí chi trả lương Nhân viên
        [sequelize.fn('SUM', sequelize.col('total_value_12')), 'total_value_12'], // Thu nhập khác
        [sequelize.fn('SUM', sequelize.col('total_value_13')), 'total_value_13'], // Phí trả hàng
        [sequelize.fn('SUM', sequelize.col('total_value_14')), 'total_value_14'], // Chi phí khác
        [sequelize.fn('SUM', sequelize.col('total_value_15')), 'total_value_15'], // Tổng giảm giá tính cả khi trả hàng
        [sequelize.fn('SUM', sequelize.col('total_value_16')), 'total_value_16'], // Tổng doanh thu tính cả khi trả hàng
        [sequelize.fn('SUM', sequelize.col('total_value_17')), 'total_value_17'], // Tổng lợi nhuận gộp tính cả khi trả hàng
        [sequelize.fn('SUM', sequelize.col('total_quantity_1')), 'total_quantity_1'], // Số đơn bán
        [sequelize.fn('SUM', sequelize.col('total_quantity_2')), 'total_quantity_2']// Số đơn trả
    ];
    if (date_time === 'year' || date_time === 'quarter') {
        newAttributes = ['month'];
        group = ['month'];
        sort = ['month', 'asc'];
    }

    if (min_created_time.getTime() === max_created_time.getTime()) {
        newAttributes = ['hours'];
        group = ['hours'];
        sort = ['hours', 'asc'];
    }

    if (type === 'return') {
        newAttributes = ['is_return', 'created_time'];
        group = ['is_return', 'created_time'];
        sort = ['is_return', 'asc'];
    }
    if ((type === 'return' && date_time === 'year') || (type === 'return' && date_time === 'quarter')) {
        newAttributes = ['is_return', 'month'];
        group = ['is_return', 'month'];
        sort = ['is_return', 'asc'];
    }

    if (type === 'discount') {
        newAttributes = ['is_discount', 'created_time'];
        group = ['is_discount', 'created_time'];
        sort = ['is_discount', 'asc'];
    }
    if ((type === 'discount' && date_time === 'year') || (type === 'discount' && date_time === 'quarter')) {
        newAttributes = ['is_discount', 'month'];
        group = ['is_discount', 'month'];
        sort = ['month', 'asc'];
    }

    if (type === 'staff') {
        newAttributes = ['created_by', 'created_time'];
        group = ['created_by', 'created_time'];
        sort = ['created_by', 'asc'];
    }
    if ((type === 'staff' && date_time === 'year') || (type === 'staff' && date_time === 'quarter')) {
        newAttributes = ['created_by', 'month'];
        group = ['created_by', 'month'];
        sort = ['month', 'asc'];
    }
    if (min_created_time.getTime() === max_created_time.getTime() && type === 'staff') {
        newAttributes = ['hours', 'created_by'];
        group = ['hours', 'created_by'];
        sort = ['hours', 'asc'];
    }

    attributes = newAttributes.concat(attributes);

    return Report.findAll({
        attributes: attributes,
        where: options,
        group: group,
        order: [sort],
        offset: skip,
        limit: limit
    });
};

/**
 * Get list sale report by time
 *
 * @public
 * @param {Parameters} params
 */
Report.listByTime = async ({
    stores,
    date_time,
    years,
    order_by,

    // sort
    skip = 0,
    limit = 20,
}) => {
    const options = filterConditions({
        stores,
        years
    });

    const sort = sortConditions({ date_time, order_by });
    const group = groupConditions(date_time);
    let newAttributes = ['month'];
    let attributes = [
        [sequelize.fn('SUM', sequelize.col('total_value_1')), 'total_value_1'], // Tổng tiền hàng không tính trả hàng
        [sequelize.fn('SUM', sequelize.col('total_value_2')), 'total_value_2'], // Tổng tiền hàng tính cả trả hàng
        [sequelize.fn('SUM', sequelize.col('total_value_3')), 'total_value_3'], // Chiết khấu hóa đơn không tính trả hàng
        [sequelize.fn('SUM', sequelize.col('total_value_4')), 'total_value_4'], // Giá trị hàng bán bị trả lại
        [sequelize.fn('SUM', sequelize.col('total_value_5')), 'total_value_5'], // Doanh thu
        [sequelize.fn('SUM', sequelize.col('total_value_6')), 'total_value_6'], // Giá vốn hàng bán
        [sequelize.fn('SUM', sequelize.col('total_value_7')), 'total_value_7'], // Chi phí voucher
        [sequelize.fn('SUM', sequelize.col('total_value_8')), 'total_value_8'], // Phí giao hàng
        [sequelize.fn('SUM', sequelize.col('total_value_9')), 'total_value_9'], // Giá trị xuất hủy
        [sequelize.fn('SUM', sequelize.col('total_value_10')), 'total_value_10'], //  Giá trị thanh toán bằng điểm
        [sequelize.fn('SUM', sequelize.col('total_value_11')), 'total_value_11'], // Phí chi trả lương Nhân viên
        [sequelize.fn('SUM', sequelize.col('total_value_12')), 'total_value_12'], // Thu nhập khác
        [sequelize.fn('SUM', sequelize.col('total_value_13')), 'total_value_13'], // Phí trả hàng
        [sequelize.fn('SUM', sequelize.col('total_value_14')), 'total_value_14'], // Chi phí khác
        [sequelize.fn('SUM', sequelize.col('total_value_15')), 'total_value_15'], // Tổng giảm giá tính cả khi trả hàng
        [sequelize.fn('SUM', sequelize.col('total_value_16')), 'total_value_16'], // Tổng doanh thu tính cả khi trả hàng
        [sequelize.fn('SUM', sequelize.col('total_value_17')), 'total_value_17'], // Tổng lợi nhuận gộp tính cả khi trả hàng
        [sequelize.fn('SUM', sequelize.col('total_quantity_1')), 'total_quantity_1'], // Số đơn bán
        [sequelize.fn('SUM', sequelize.col('total_quantity_2')), 'total_quantity_2']// Số đơn trả
    ];

    if (date_time === 'year') {
        newAttributes = ['year'];
    }

    if (date_time === 'quarter') {
        newAttributes = ['quarter'];
    }

    if (date_time === 'month' || !date_time) {
        newAttributes = ['month'];
    }

    attributes = newAttributes.concat(attributes);

    return Report.findAll({
        attributes: attributes,
        where: options,
        group: group,
        order: [sort],
        offset: skip,
        limit: limit
    });
};

/**
 * Sum quantity items list records
 *
 * @public
 * @param {Parameters} params
 */
Report.sumRecords = async ({
    staffs,
    price_book,
    channels,
    stores,
    type,
    is_delivery,
    years,
    min_created_time,
    max_created_time
}) => {
    try {
        const options = filterConditions({
            staffs,
            price_book,
            channels,
            stores,
            type,
            years,
            is_delivery,
            min_created_time,
            max_created_time
        });
        const total_quantity_1 = await Report.sum(
            'total_quantity_1',
            {
                where: options,
            }
        );
        const total_quantity_2 = await Report.sum(
            'total_quantity_2',
            {
                where: options,
            }
        );
        const total_value_1 = await Report.sum(
            'total_value_1',
            {
                where: options,
            }
        );
        const total_value_2 = await Report.sum(
            'total_value_2',
            {
                where: options,
            }
        );
        const total_value_3 = await Report.sum(
            'total_value_3',
            {
                where: options,
            }
        );
        const total_value_4 = await Report.sum(
            'total_value_4',
            {
                where: options,
            }
        );
        const total_value_5 = await Report.sum(
            'total_value_5',
            {
                where: options,
            }
        );
        const total_value_6 = await Report.sum(
            'total_value_6',
            {
                where: options,
            }
        );
        const total_value_7 = await Report.sum(
            'total_value_7',
            {
                where: options,
            }
        );
        const total_value_8 = await Report.sum(
            'total_value_8',
            {
                where: options,
            }
        );
        const total_value_9 = await Report.sum(
            'total_value_9',
            {
                where: options,
            }
        );
        const total_value_10 = await Report.sum(
            'total_value_10',
            {
                where: options,
            }
        );
        const total_value_11 = await Report.sum(
            'total_value_11',
            {
                where: options,
            }
        );
        const total_value_12 = await Report.sum(
            'total_value_12',
            {
                where: options,
            }
        );
        const total_value_13 = await Report.sum(
            'total_value_13',
            {
                where: options,
            }
        );
        const total_value_14 = await Report.sum(
            'total_value_14',
            {
                where: options,
            }
        );
        const total_value_15 = await Report.sum(
            'total_value_15',
            {
                where: options,
            }
        );
        const total_value_16 = await Report.sum(
            'total_value_16',
            {
                where: options,
            }
        );
        const total_value_17 = await Report.sum(
            'total_value_17',
            {
                where: options,
            }
        );
        // console.log(total_amount);
        return {
            total_value_1,
            total_value_2,
            total_value_3,
            total_value_4,
            total_value_5,
            total_value_6,
            total_value_7,
            total_value_8,
            total_value_9,
            total_value_10,
            total_value_11,
            total_value_12,
            total_value_13,
            total_value_14,
            total_value_15,
            total_value_16,
            total_value_17,
            total_quantity_1,
            total_quantity_2
        };
    } catch (ex) {
        throw ex;
    }
};

/**
 * Filter only allowed fields from Report
 *
 * @param {Object} params
 */
Report.filterParams = (params) => pick(params, PUBLIC_FIELDS);

/**
 * @typedef Report
 */
export default Report;
