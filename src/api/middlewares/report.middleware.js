import { handler as ErrorHandler } from './error';
import Report from '../../common/models/report.model';

/**
 * Load sum for filter.
 */
exports.sum = async (req, res, next) => {
    try {
        const sum = await Report.sumRecords(req.query);
        req.locals = req.locals ? req.locals : {};
        req.locals.sum = sum;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};
