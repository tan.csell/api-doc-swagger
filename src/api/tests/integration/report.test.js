/* eslint-disable no-undef */
/* eslint-disable arrow-body-style */
/* eslint-disable no-unused-expressions */
import { Op } from 'sequelize';
import request from 'supertest';
import httpStatus from 'http-status';
import { expect } from 'chai';
import { omit } from 'lodash';
import app from '../../../index';
import Report from '../../../common/models/report.model';

const token = process.env.BEARER_TOKEN;
describe('Report API', async () => {
    let records;
    let newRecord;

    beforeEach(async () => {
        records = [
            {
                url: 'https://web-ccs.csell.com.vn/',
                title: 'Deal ủng hộ đồng bào miền trung',
                content: 'Deal ủng hộ đồng bào miền trung',
                position: 1,
                image_url: 'https://cdn-thumb-image-ccs.csell.com.vn/2020/10/reports/1603877579591-hot_deal4.jpeg',
                mobile_url: 'https://cdn-thumb-image-ccs.csell.com.vn/2020/10/reports/1603877579591-hot_deal4.jpeg',
                is_visible: true
            },
            {
                url: 'https://web-ccs.csell.com.vn/',
                title: 'Deal ủng hộ đồng bào miền trung',
                content: 'Deal ủng hộ đồng bào miền trung',
                position: 1,
                image_url: 'https://cdn-thumb-image-ccs.csell.com.vn/2020/10/reports/1603877579591-hot_deal4.jpeg',
                mobile_url: 'https://cdn-thumb-image-ccs.csell.com.vn/2020/10/reports/1603877579591-hot_deal4.jpeg',
                is_visible: false
            },
            {
                url: 'https://web-ccs.csell.com.vn/',
                title: 'Deal ủng hộ đồng bào miền trung',
                content: 'Deal ủng hộ đồng bào miền trung',
                position: 1,
                image_url: 'https://cdn-thumb-image-ccs.csell.com.vn/2020/10/reports/1603877579591-hot_deal4.jpeg',
                mobile_url: 'https://cdn-thumb-image-ccs.csell.com.vn/2020/10/reports/1603877579591-hot_deal4.jpeg',
                is_visible: false
            }
        ];
        newRecord = {
            url: 'https://web-ccs.csell.com.vn/',
            title: 'Deal ủng hộ đồng bào miền trung',
            content: 'Deal ủng hộ đồng bào miền trung',
            position: 1,
            image_url: 'https://cdn-thumb-image-ccs.csell.com.vn/2020/10/reports/1603877579591-hot_deal4.jpeg',
            mobile_url: 'https://cdn-thumb-image-ccs.csell.com.vn/2020/10/reports/1603877579591-hot_deal4.jpeg',
            is_visible: true
        };
        await Report.destroy({
            where: { id: { [Op.ne]: null } }
        });
        await Report.bulkCreate(records);
    });

    describe('GET /v1/reports', () => {
        it('should get all wards', () => {
            return request(app)
                .get('/v1/reports')
                .set('Authorization', token)
                .expect('Content-Type', /json/)
                .expect(httpStatus.OK)
                .then((res) => {
                    expect(res.body.count).to.be.an('number');
                    expect(res.body.count).to.be.have.eq(3);

                    expect(res.body.data).to.be.an('array');
                    expect(res.body.data).to.have.lengthOf(3);
                    console.log('ok');
                });
        });
        it('should get all wards with skip and limit', () => {
            return request(app)
                .get('/v1/reports')
                .set('Authorization', token)
                .query({ skip: 2, limit: 10 })
                .expect('Content-Type', /json/)
                .expect(httpStatus.OK)
                .then((res) => {
                    expect(res.body.count).to.be.an('number');
                    expect(res.body.count).to.be.have.eq(3);

                    expect(res.body.data).to.be.an('array');
                    expect(res.body.data).to.have.lengthOf(1);
                    console.log('ok');
                });
        });
        it('should report error when skip is not a number', () => {
            return request(app)
                .get('/v1/reports')
                .set('Authorization', token)
                .query({ skip: 'asdasd', limit: 20 })
                .expect('Content-Type', /json/)
                .expect(httpStatus.OK)
                .then((res) => {
                    const { field, location, messages } = res.body.errors[0];
                    expect(res.body.code).to.be.equal(400);
                    expect(field).to.be.equal('skip');
                    expect(location).to.be.equal('query');
                    expect(messages).to.include('"skip" must be a number');
                    console.log('ok');
                });
        });
        it('should report error when limit is not a number', () => {
            return request(app)
                .get('/v1/reports')
                .set('Authorization', token)
                .query({ skip: 0, limit: 'dasdasdads' })
                .expect('Content-Type', /json/)
                .expect(httpStatus.OK)
                .then((res) => {
                    const { field, location, messages } = res.body.errors[0];
                    expect(res.body.code).to.be.equal(400);
                    expect(field).to.be.equal('limit');
                    expect(location).to.be.equal('query');
                    expect(messages).to.include('"limit" must be a number');
                    console.log('ok');
                });
        });
        it('should get all banner with params types = home_banner_v_1', () => {
            return request(app)
                .get('/v1/reports')
                .set('Authorization', token)
                .query({ types: 'home_banner_v_1' })
                .expect('Content-Type', /json/)
                .expect(httpStatus.OK)
                .then((res) => {
                    expect(res.body.data).to.be.an('array');
                    expect(res.body.data).to.have.lengthOf(3);
                    console.log('ok');
                });
        });
        it('should get all banner with params types = home_banner_v_2', () => {
            return request(app)
                .get('/v1/reports')
                .set('Authorization', token)
                .query({ types: 'home_banner_v_2' })
                .expect('Content-Type', /json/)
                .expect(httpStatus.OK)
                .then((res) => {
                    expect(res.body.data).to.be.an('array');
                    expect(res.body.data).to.have.lengthOf(0);
                    console.log('ok');
                });
        });
    });

    describe('POST /v1/reports', () => {
        it('should create a new banner when request is ok', () => {
            return request(app)
                .post('/v1/reports')
                .set('Authorization', token)
                .send(newRecord)
                .expect('Content-Type', /json/)
                .expect(httpStatus.OK)
                .then((res) => {
                    expect(res.body.code).to.equal(0);
                    console.log('ok');
                });
        });
        it('should report error when required fields is not provided', () => {
            const requiredFields = [
                'url',
                'image_url',
                'mobile_url'
            ];
            newRecord = omit(newRecord, requiredFields);
            return request(app)
                .post('/v1/reports')
                .set('Authorization', token)
                .send(newRecord)
                .expect('Content-Type', /json/)
                .expect(httpStatus.OK)
                .then((res) => {
                    for (
                        let index = 0;
                        index < requiredFields.length;
                        index += 1
                    ) {
                        const field = requiredFields[index];
                        expect(res.body.errors[index].field).to.be.equal(
                            `${field}`
                        );
                        expect(res.body.errors[index].location).to.be.equal(
                            'body'
                        );
                        expect(res.body.errors[index].messages).to.include(
                            `"${field}" is required`
                        );
                    }
                });
        });
        it('should create a new banner and set default values', () => {
            const defaultValues = [
                'type',
                'title',
                'content',
                'position',
                'is_visible'
            ];
            newRecord = omit(
                newRecord, defaultValues
            );
            return request(app)
                .post('/v1/reports')
                .set('Authorization', token)
                .send(newRecord)
                .expect('Content-Type', /json/)
                .expect(httpStatus.OK)
                .then((res) => {
                    const {
                        created_at,
                        updated_at
                    } = res.body.data;
                    expect(res.body.code).to.equal(0);
                    expect(created_at).to.be.an('number');
                    expect(updated_at).to.be.an('number');
                });
        });
    });

    describe('PUT /v1/reports/:id', () => {
        it('should report error when update a banner with incorrect id', () => {
            return request(app)
                .put('/v1/reports/9999999')
                .set('Authorization', token)
                .expect('Content-Type', /json/)
                .expect(httpStatus.OK)
                .then((res) => {
                    expect(res.body.code).to.equal(404);
                    expect(res.body.message).to.equal('Không tìm thấy banner!');
                    console.log('ok');
                });
        });
        it('should update correct url banner', async () => {
            const banner = await Banner.findOne({
                order: [
                    ['id', 'asc']
                ]
            });
            return request(app)
                .put(`/v1/reports/${banner.id}`)
                .set('Authorization', token)
                .send({ url: 'url 2 update' })
                .expect('Content-Type', /json/)
                .expect(httpStatus.OK)
                .then((res) => {
                    expect(res.body.code).to.equal(0);
                    console.log('ok');
                });
        });
        it('should report error when incorrect url banner', async () => {
            const banner = await Banner.findOne({
                order: [
                    ['id', 'asc']
                ]
            });
            return request(app)
                .put(`/v1/reports/${banner.id}`)
                .set('Authorization', token)
                .send({ url: {} })
                .expect('Content-Type', /json/)
                .expect(httpStatus.OK)
                .then((res) => {
                    const { field, location, messages } = res.body.errors[0];
                    expect(res.body.code).to.be.equal(400);
                    expect(field).to.be.equal('url');
                    expect(location).to.be.equal('body');
                    expect(messages).to.include('"url" must be a string');
                    console.log('ok');
                });
        });
        it('should update correct title banner', async () => {
            const banner = await Banner.findOne({
                order: [
                    ['id', 'asc']
                ]
            });
            return request(app)
                .put(`/v1/reports/${banner.id}`)
                .set('Authorization', token)
                .send({ title: 'title 2 update' })
                .expect('Content-Type', /json/)
                .expect(httpStatus.OK)
                .then((res) => {
                    expect(res.body.code).to.equal(0);
                    console.log('ok');
                });
        });
        it('should report error when incorrect title banner', async () => {
            const banner = await Banner.findOne({
                order: [
                    ['id', 'asc']
                ]
            });
            return request(app)
                .put(`/v1/reports/${banner.id}`)
                .set('Authorization', token)
                .send({ title: {} })
                .expect('Content-Type', /json/)
                .expect(httpStatus.OK)
                .then((res) => {
                    const { field, location, messages } = res.body.errors[0];
                    expect(res.body.code).to.be.equal(400);
                    expect(field).to.be.equal('title');
                    expect(location).to.be.equal('body');
                    expect(messages).to.include('"title" must be a string');
                    console.log('ok');
                });
        });
        it('should update correct content banner', async () => {
            const banner = await Banner.findOne({
                order: [
                    ['id', 'asc']
                ]
            });
            return request(app)
                .put(`/v1/reports/${banner.id}`)
                .set('Authorization', token)
                .send({ content: 'content 2 update' })
                .expect('Content-Type', /json/)
                .expect(httpStatus.OK)
                .then((res) => {
                    expect(res.body.code).to.equal(0);
                    console.log('ok');
                });
        });
        it('should report error when incorrect content banner', async () => {
            const banner = await Banner.findOne({
                order: [
                    ['id', 'asc']
                ]
            });
            return request(app)
                .put(`/v1/reports/${banner.id}`)
                .set('Authorization', token)
                .send({ content: {} })
                .expect('Content-Type', /json/)
                .expect(httpStatus.OK)
                .then((res) => {
                    const { field, location, messages } = res.body.errors[0];
                    expect(res.body.code).to.be.equal(400);
                    expect(field).to.be.equal('content');
                    expect(location).to.be.equal('body');
                    expect(messages).to.include('"content" must be a string');
                    console.log('ok');
                });
        });
        it('should update correct position banner', async () => {
            const banner = await Banner.findOne({
                order: [
                    ['id', 'asc']
                ]
            });
            return request(app)
                .put(`/v1/reports/${banner.id}`)
                .set('Authorization', token)
                .send({ position: 3 })
                .expect('Content-Type', /json/)
                .expect(httpStatus.OK)
                .then((res) => {
                    expect(res.body.code).to.equal(0);
                    console.log('ok');
                });
        });
        it('should report error when incorrect position banner', async () => {
            const banner = await Banner.findOne({
                order: [
                    ['id', 'asc']
                ]
            });
            return request(app)
                .put(`/v1/reports/${banner.id}`)
                .set('Authorization', token)
                .send({ position: {} })
                .expect('Content-Type', /json/)
                .expect(httpStatus.OK)
                .then((res) => {
                    const { field, location, messages } = res.body.errors[0];
                    expect(res.body.code).to.be.equal(400);
                    expect(field).to.be.equal('position');
                    expect(location).to.be.equal('body');
                    expect(messages).to.include('"position" must be a number');
                    console.log('ok');
                });
        });
        it('should update correct image_url banner', async () => {
            const banner = await Banner.findOne({
                order: [
                    ['id', 'asc']
                ]
            });
            return request(app)
                .put(`/v1/reports/${banner.id}`)
                .set('Authorization', token)
                .send({ image_url: 'image_url 2 update' })
                .expect('Content-Type', /json/)
                .expect(httpStatus.OK)
                .then((res) => {
                    expect(res.body.code).to.equal(0);
                    console.log('ok');
                });
        });
        it('should report error when incorrect image_url banner', async () => {
            const banner = await Banner.findOne({
                order: [
                    ['id', 'asc']
                ]
            });
            return request(app)
                .put(`/v1/reports/${banner.id}`)
                .set('Authorization', token)
                .send({ image_url: {} })
                .expect('Content-Type', /json/)
                .expect(httpStatus.OK)
                .then((res) => {
                    const { field, location, messages } = res.body.errors[0];
                    expect(res.body.code).to.be.equal(400);
                    expect(field).to.be.equal('image_url');
                    expect(location).to.be.equal('body');
                    expect(messages).to.include('"image_url" must be a string');
                    console.log('ok');
                });
        });
        it('should update correct mobile_url banner', async () => {
            const banner = await Banner.findOne({
                order: [
                    ['id', 'asc']
                ]
            });
            return request(app)
                .put(`/v1/reports/${banner.id}`)
                .set('Authorization', token)
                .send({ mobile_url: 'mobile_url 2 update' })
                .expect('Content-Type', /json/)
                .expect(httpStatus.OK)
                .then((res) => {
                    expect(res.body.code).to.equal(0);
                    console.log('ok');
                });
        });
        it('should report error when incorrect mobile_url banner', async () => {
            const banner = await Banner.findOne({
                order: [
                    ['id', 'asc']
                ]
            });
            return request(app)
                .put(`/v1/reports/${banner.id}`)
                .set('Authorization', token)
                .send({ mobile_url: {} })
                .expect('Content-Type', /json/)
                .expect(httpStatus.OK)
                .then((res) => {
                    const { field, location, messages } = res.body.errors[0];
                    expect(res.body.code).to.be.equal(400);
                    expect(field).to.be.equal('mobile_url');
                    expect(location).to.be.equal('body');
                    expect(messages).to.include('"mobile_url" must be a string');
                    console.log('ok');
                });
        });
        it('should update correct is_visible banner', async () => {
            const banner = await Banner.findOne({
                order: [
                    ['id', 'asc']
                ]
            });
            return request(app)
                .put(`/v1/reports/${banner.id}`)
                .set('Authorization', token)
                .send({ is_visible: false })
                .expect('Content-Type', /json/)
                .expect(httpStatus.OK)
                .then((res) => {
                    expect(res.body.code).to.equal(0);
                    console.log('ok');
                });
        });
        it('should report error when incorrect is_visible banner', async () => {
            const banner = await Banner.findOne({
                order: [
                    ['id', 'asc']
                ]
            });
            return request(app)
                .put(`/v1/reports/${banner.id}`)
                .set('Authorization', token)
                .send({ is_visible: 2 })
                .expect('Content-Type', /json/)
                .expect(httpStatus.OK)
                .then((res) => {
                    const { field, location, messages } = res.body.errors[0];
                    expect(res.body.code).to.be.equal(400);
                    expect(field).to.be.equal('is_visible');
                    expect(location).to.be.equal('body');
                    expect(messages).to.include('"is_visible" must be a boolean');
                    console.log('ok');
                });
        });
    });
});
