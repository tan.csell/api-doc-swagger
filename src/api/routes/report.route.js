import express from 'express';
import validate from 'express-validation';
import { authorize } from 'auth-adapter';
import Permissions from '../../common/utils/Permissions';
import controller from '../controllers/v1/report.controller';
import middleware from '../middlewares/report.middleware';

import {
    listValidation,
} from '../validations/reoport.validation';

const router = express.Router();

router
    .route('/')
    .get(
        validate(listValidation),
        authorize([Permissions.LOGGED_IN]),
        middleware.sum,
        controller.list
    );
export default router;
