import SaleReport from '../../../common/models/report.model';
import { handler as ErrorHandler } from '../../middlewares/error';

/**
 * List sale report by time
 *
 * @public
 * @param query
 * @returns {Promise<SaleReport[]>, APIError>}
 */
exports.list = async (req, res, next) => {
    SaleReport.list(
        req.query
    ).then(result => {
        res.json({
            code: 0,
            sum: req.locals.sum,
            data: result.map(s => SaleReport.transform(s))
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * List financial report by time
 *
 * @public
 * @param query
 * @returns {Promise<SaleReport[]>, APIError>}
 */
exports.listFinancialReport = async (req, res, next) => {
    SaleReport.listByTime(
        req.query
    ).then(result => {
        res.json({
            code: 0,
            sum: req.locals.sum,
            data: result.map(s => SaleReport.transform(s))
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};
