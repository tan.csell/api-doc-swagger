import Joi from 'joi';
import { values } from 'lodash';

module.exports = {
    // GET v1/imports
    listValidation: {
        query: {
            // pagging
            skip: Joi.number()
                .min(0)
                .allow(null, ''),
            limit: Joi.number()
                .min(1)
                .max(10000)
                .allow(null, ''),
            // search
            staffs: Joi.string()
                .trim()
                .allow(null, ''),
            stores: Joi.string()
                .trim()
                .allow(null, ''),
            price_book: Joi.string()
                .trim()
                .allow(null, ''),
            type: Joi.string()
                .trim()
                .allow(null, ''),
            channels: Joi.string()
                .trim()
                .allow(null, ''),
            min_created_time: Joi.date()
                .allow(null, ''),
            max_created_time: Joi.date()
                .allow(null, ''),
            year: Joi.number().integer()
                .allow(null, ''),
            is_delivery: Joi.string()
                .trim()
                .only(values(['true', 'false', 'true,false', 'true, false']))
                .allow(null, ''),
        }
    }
};
