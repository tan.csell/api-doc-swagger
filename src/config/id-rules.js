export default {
    orderCode: {
        length: 4,
        alphabet: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    }
};
