import { serviceName } from './vars';

const types = {
    FINISH_PACKAGE_FULFILLMENT: `${serviceName}-finish-package-fulfillment`,
};

export default types;
